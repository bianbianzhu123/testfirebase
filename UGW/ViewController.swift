//
//  ViewController.swift
//  UGW
//
//  Created by TIANYI LI on 5/12/19.
//  Copyright © 2019 TIANYI LI. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController {

    @IBOutlet weak var homeButton: UIButton!
    @IBOutlet weak var uniButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var enterYD: UITextField!
    
    
    let locationManager = CLLocationManager()
    let regionInMeters: Double = 10000
    let defaultRedColor = (UIColor(red: 234.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        homeButton.layer.borderWidth = 3
        uniButton.layer.borderWidth = 3
        homeButton.layer.cornerRadius = 25
        uniButton.layer.cornerRadius = 25
        homeButton.layer.borderColor = defaultRedColor.cgColor
        homeButton.setTitleColor(UIColor.sosRed, for: .normal)
        
        
        uniButton.layer.backgroundColor = defaultRedColor.cgColor
        uniButton.layer.borderColor = UIColor.white.cgColor
        searchButton.layer.borderWidth = 1
        homeButton.layer.shadowRadius = 8
        homeButton.layer.shadowOpacity = 0.5
        homeButton.layer.shadowOffset = CGSize(width: 1, height: 1)
        uniButton.layer.shadowRadius = 8
        uniButton.layer.shadowOpacity = 0.5
        uniButton.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        
        
        
        checkLocationServices()
        
        
    }
    
    
    
    
    
    @IBAction func homeButtonPressed(_ sender: UIButton) {
        
        
        homeButton.layer.backgroundColor = defaultRedColor.cgColor
        homeButton.setTitleColor(.white, for: .normal)
        uniButton.layer.borderColor = UIColor(red: 234.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0).cgColor
        uniButton.setTitleColor(UIColor.sosRed, for: .normal)
        uniButton.layer.backgroundColor = UIColor.white.cgColor
        
        
    }
    
    
    @IBAction func uniButtonPressed(_ sender: UIButton) {
        homeButton.layer.backgroundColor = UIColor.white.cgColor
        homeButton.setTitleColor(.sosRed, for: .normal)
        homeButton.layer.borderColor = UIColor(red: 234.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0).cgColor
        uniButton.setTitleColor(UIColor.white, for: .normal)
        uniButton.layer.backgroundColor = defaultRedColor.cgColor
    }
    
    
    
    
    
    
    
    func setupLocationManager() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
    
    }
    
    func centerViewOnUserLocation() {
        if let location = locationManager.location?.coordinate {
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters)
            mapView.setRegion(region, animated: true)
            //show user's location (center)
        }
    }
    
    
    func checkLocationServices(){
        if CLLocationManager.locationServicesEnabled(){
            setupLocationManager()
            checkLocationAuthorization()
            //setup our location manager
        } else {
            // show alert letting the user know they have to turn this on.
        }
    }
    
    func checkLocationAuthorization() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            //do map stuff
            mapView.showsUserLocation = true
            centerViewOnUserLocation()
            locationManager.startUpdatingLocation()
            break
        case .denied:
            // show alert instructing them how to turn on permissions
            break
        case .notDetermined:
            locationManager .requestWhenInUseAuthorization()
        case .restricted:
            // show alert leting them know what's up
            break
        case .authorizedAlways:
            break
            //cannot do that as people won't be happy with this privacy setting
        }
    }
}

extension ViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion.init(center:center, latitudinalMeters: regionInMeters, longitudinalMeters: regionInMeters
        )
        mapView.setRegion(region, animated: true)
        //update the user locatoins
        
    }
        //
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
    }
}



extension UIColor {
    static var sosRed = UIColor.init(red: 234.0/255.0, green: 74.0/255.0, blue: 74.0/255.0, alpha: 1.0)
    
}
