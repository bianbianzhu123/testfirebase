//
//  ViewController.swift
//  UGW
//
//  Created by TIANYI LI on 12/12/19.
//  Copyright © 2019 TIANYI LI. All rights reserved.
//

import UIKit

class OptionList: UIViewController {

    @IBOutlet weak var optionListView2nd: UIView!
    @IBOutlet weak var optionView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        optionView.layer.cornerRadius = 15
        optionView.layer.shadowRadius = 8
        optionView.layer.shadowOpacity = 0.5
        optionView.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        
        optionListView2nd.layer.shadowRadius = 8
        optionListView2nd.layer.shadowOpacity = 0.5
        optionListView2nd.layer.shadowOffset = CGSize(width: 1, height: 1)
        
        
        
        
        
        

        // Do any additional setup after loading the view.
    }
    

  
    @IBAction func optionBackwardPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)

    }
    

}
